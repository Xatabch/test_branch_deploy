const express = require('express');

const app = express();
const port = 8000;

app.get('/ping', (req, res) => {
    res.status(200).send({"ping": "pong"});
});

app.listen(port, () => {
    console.log(`Application is listening on port: ${port}`);
})
